Definición de una máquina virtual con Vagrant para desplegar un Servidor Web

En este ejemplo utilizaremos un entonro vagrant para ejecutar un servidor web. Cuano se 
desarrollan aplicaciones web, Vagrant se puede usar para crear una máquina virtual para 
el entorno de tiempo de ejecución de la aplicación.  Para poder hacer pruebas de nuestra 
aplicación lo mas como seria utilizar un navegador que se ejecute sobre el sistema 
operativo anfitrión. Para lograr esto deberemos configurar el reenvio de puertos para el 
sistema invitado (vm con Vagrant). El reenvío de puertos nos permite reenviar un puerto 
en la máquina Vagrant a un puerto en la máquina host. Por ejemplo, el reenvío del puerto 80 
(el puerto HTTP estándar) en la máquina Vagrant al puerto 8000 en la máquina host, esto 
nos permite acceder a un servidor web en la máquina virtual accediendo a la url:

  http://localhost:8000 

Esto puede ser útil para administrar e interactuar con un entorno de desarrollo en una 
máquina Vagrant.

Que utilizaremos:
  1- Archivo Vagrantfile
  2- Una box de debian/buster64

Pasos: 
  1- Crear carpeta de trabajo
      Cree una carpeta de trabajo:
        $ mkdir ej2_vm1
        $ cd ej2_vm1
  2- Crear archivo Vagrantfile
        $ vagrant init --minimal debian/buster64
  3- Configurar y hacer ajustes al arhcivo Vagrantfile
      Para lograr nuestro objetivo ocuparemos:
        config.vm.network "forwarded_port", guest: 80, host: 8000
  4- Iniciar entorno
        $ vagrant up
  5- Comprobar estado de la maquina
        $ vagrant status
  6- Login en la máquina virtual
        $ vagrant ssh
  7- Instalar servidor web nginx
        # apt update && apt install nginx
  8- Probar si el servidor web esta running!
        Haga uso de systemctol o service, segun sea el caso
  9- Hacer pruebas con un navegador en el equipo anfitrión
        http://localhost:8000 
  10- Apagado de la máquina virtual
        $ vagrant halt
  11- Destruir la máquina (opcional)
        $ vagrant destroy
