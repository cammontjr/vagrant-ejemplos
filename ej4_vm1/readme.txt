Definición de una máquina virtual con Vagrant y configurar el Provider (Virtualbox)
desde el archivo de Vagrantfile.

Hasta este momento, todos los entornos iniciados se han controlado mediante la 
configuración predeterminada o la configuración que se proporciona en los boxes.
Esto a veces está bien, pero a menudo es deseable usar el Vagrantfile para 
controlar la configuración en la máquina virtual para permitir que la máquina 
virtual se ejecute con mas eficiencia en el equipo anfitrión. 
Lo mas común cambiar los parámetros de memoria RAM y la cantidad de nucleos o 
proesadores asignados. De igual manera se podria pensar en otros parametros de
configuración y agregar mas recursos de hardware.


Que utilizaremos:
  1- Archivo Vagrantfile
  2- Una box de debian/buster64

Pasos: 
  1- Crear carpeta de trabajo
      Cree una carpeta de trabajo:
        $ mkdir ej4_vm1
        $ cd ej4_vm1
  2- Crear archivo Vagrantfile
        $ vagrant init --minimal debian/buster64
  3- Configurar y hacer ajustes al arhcivo Vagrantfile
      Para lograr nuestro objetivo utilizaremos:
        config.vm.provider "virtualbox" do |vbox|
          vbox.memory = "2048"
          vbox.cpus = 2
        end
  4- Iniciar entorno
        $ vagrant up
  5- Comprobar estado de la maquina
        $ vagrant status
  6- Login en la máquina virtual
        $ vagrant ssh
  7- Verificar los recursos asignandos al máquina virtual
        Para esto puede utilizar top, htop, free, vmstat 
        entre otros comandos para verificar en el sistema 
        invitado la configuracion de hardware
  8- Apagado de la máquina virtual
        $ vagrant halt
  9- Destruir la máquina (opcional)
        $ vagrant destroy
